
create database proyectoDB

-- Sin fk
CREATE TABLE agr_Categoriaproducto(
    IDcategoriaprod int not null PRIMARY KEY, 
	Nombre varchar(50) not null,
);

CREATE TABLE agr_Categoriausuario(
	IDcategoria int not null PRIMARY KEY,
	Nombre varchar(50) not null
);

CREATE TABLE ee_Serviciopaqueteria(
    IDpaqueteria int not null PRIMARY KEY,
	Nombre varchar(50) not null,
	Direccion varchar(100) not null,
	Telefono varchar(10) not null
);

CREATE TABLE p_Empresa(
	IDempresa varchar(5) not null PRIMARY KEY,
	Nombre varchar(50) not null
);

CREATE TABLE agr_Productos(
    IDproducto varchar(5) not null PRIMARY KEY,
	Nombre varchar(30) not null,
	Stock int not null CHECK(Stock >= 0),
	Precio decimal (5,2) not null CHECK(Precio > 0)
);

CREATE TABLE agr_Reporteproducto(
	IDreporteprod int not null PRIMARY KEY,
	Nombre varchar(50) not null,
	Cantidad int not null CHECK(Cantidad > 0),
	Descripcion varchar(200) not null,
	Fecha date not null
);
--------------------------------------------------------------------------------------
--Con fk
CREATE TABLE agr_Usuario(
    IDusuario int not null PRIMARY KEY,
	Correo varchar(50) not null unique,
	Contraseña varchar(50) not null,
	RolUsuario int --fk categoriausuario
);
ALTER TABLE agr_Usuario
ADD CONSTRAINT FK_Usuario_Categoriausuario
FOREIGN KEY (RolUsuario) REFERENCES agr_Categoriausuario(IDcategoria);

CREATE TABLE agr_Almacen(
    IDalmacen varchar(5) not null PRIMARY KEY,
	Nombre varchar(30) not null,
	Estatus varchar (20) not null,
	Direccion varchar (100) not null,
	CapacidadCarga int not null CHECK(CapacidadCarga > 0),
	Telefono varchar(10) not null,
	Usuario int, --fk hacia usuario
	CHECK (Estatus IN('Disponible','No disponible'))
);
ALTER TABLE agr_Almacen
ADD CONSTRAINT FK_Almacen_Usuario
FOREIGN KEY (Usuario) REFERENCES agr_Usuario(IDusuario);

CREATE TABLE p_Proveedor(
	IDproveedor int not null PRIMARY KEY,
	NombrePila varchar(50) not null,
	ApellidoPat varchar(50) not null,
	ApellidoMat varchar(50) not null,
	Telefono varchar(10) not null,
	Empresa varchar(5), --fk hacia empresa
	Usuario int --fk hacia usuario
);
ALTER TABLE p_Proveedor
ADD CONSTRAINT FK_Proveedor_Empresa
FOREIGN KEY (Empresa) REFERENCES p_Empresa(IDempresa);
ALTER TABLE p_Proveedor
ADD CONSTRAINT FK_Proveedor_Usuario
FOREIGN KEY (Usuario) REFERENCES agr_Usuario(IDusuario);

CREATE TABLE agr_Detalleproductos(
	IDdetalleproducto int not null primary key,
	Peso int not null,
	Estado varchar(20) not null,
	Tamaño varchar(10) not null,
	Modelo varchar(100) not null,
	Marca varchar(20) not null,
	Producto varchar(5) -- fk hacia productos
);
alter table agr_Detalleproductos
add constraint FK_Detalleproductos_Productos
foreign key(Producto) references agr_Productos(IDproducto);

CREATE TABLE agr_Fotos(
	IDfoto int not null primary key,
	Link varchar(50) not null,
	Producto varchar(5) --fk hacia productos
);
alter table agr_Fotos
add constraint FK_Fotos_Productos
foreign key(Producto) references agr_Productos(IDproducto);

CREATE TABLE p_Ordenenvio(
	IDordenenvio int not null,
	Destinatario varchar(50) not null,
	Estatus varchar(20) not null,
	Fecha date not null,
	CodigoPostal varchar(5) not null,
	Calle varchar(50) not null,
	NumInt varchar(10) not null,
	NumExt varchar(10) not null,
	Colonia varchar(50) not null,
	Proveedor int, --fk hacia proveedor
	Almacen varchar(5), -- fk hacia almacen
	Producto varchar(5), -- fk hacia almacen
	CHECK (Estatus IN ('Entregado', 'Cancelado', 'En camino', 'Con retraso'))
);
ALTER TABLE p_Ordenenvio
ADD CONSTRAINT FK_Ordenenvio_Proveedor
FOREIGN KEY (Proveedor) REFERENCES p_Proveedor(IDproveedor);
ALTER TABLE p_Ordenenvio
ADD CONSTRAINT FK_Ordenenvio_Almacen
FOREIGN KEY (Almacen) REFERENCES agr_Almacen(IDalmacen);
ALTER TABLE p_Ordenenvio
ADD CONSTRAINT FK_Ordenenvio_Producto
FOREIGN KEY (Producto) REFERENCES agr_Productos(IDproducto);

CREATE TABLE ee_Envio(
    IDenvio int not null PRIMARY KEY,
	Fecha date not null,
	Hora time not null,
	Paqueteria int --fk hacia ee_serviciopaqueteria
);
ALTER TABLE ee_Envio
ADD CONSTRAINT FK_Envio_Serviciopaqueteria
FOREIGN KEY (Paqueteria) REFERENCES ee_Serviciopaqueteria(IDpaqueteria);

CREATE TABLE ee_Ruta(
    IDruta int not null PRIMARY KEY,
	Hora time not null,
	Fecha date not null,
	Direccion varchar(100) not null,
	Envio int -- fk hacia envio
);
alter table ee_Ruta
add constraint FK_Ruta_Envio
foreign key (envio) references ee_Envio(IDenvio);

CREATE TABLE ee_Entrega(
    IDentrega int not null PRIMARY KEY,
	Fecha date not null,
	Hora time not null,
	Envio int --fk hacia ee_envio
);
ALTER TABLE ee_Entrega
ADD CONSTRAINT FK_Entrega_Envio
FOREIGN KEY (Envio) REFERENCES ee_Envio(IDenvio);

CREATE TABLE agr_Reporteenvio(
    IDreporteenv int not null PRIMARY KEY,
	Estatus varchar(15) not null,
	Cantidad int not null CHECK (Cantidad > 0),
	Descripcion varchar(200)not null,
	Almacen varchar(5), --fk hacia almacen
	Envio int, --fk hacia ee_envio
	Producto varchar(5), --fk hacia producto
	CHECK (Estatus IN ('Entregado', 'Cancelado', 'En camino', 'Con retraso'))
);
ALTER TABLE agr_Reporteenvio
ADD CONSTRAINT FK_Reporteenvio_Almacen
FOREIGN KEY (Almacen) REFERENCES agr_Almacen(IDalmacen);
ALTER TABLE agr_Reporteenvio
ADD CONSTRAINT FK_Reporteenvio_Envio
FOREIGN KEY (Envio) REFERENCES ee_Envio(IDenvio);
alter table agr_Reporteenvio
add constraint FK_Reporteenvio_Productos
foreign key (Producto) references agr_Productos(IDproducto);

CREATE TABLE agr_Reporteentrega(
    IDreportent int not null PRIMARY KEY,
	Descripcion varchar(200) not null,
	Estatus varchar(15) not null,
	Cantidad int not null CHECK (Cantidad > 0),
	Almacen varchar(5), --fk hacia almacen
	Entrega int, -- fk hacia entrega
	Producto varchar(5), --fk hacia producto
	CHECK (Estatus IN ('Entregado', 'Cancelado'))
);
ALTER TABLE agr_Reporteentrega
ADD CONSTRAINT FK_Reporteentrega_Almacen
FOREIGN KEY (Almacen) REFERENCES agr_Almacen(IDalmacen);
ALTER TABLE agr_Reporteentrega
ADD CONSTRAINT FK_Reporteentrega_Entrega
FOREIGN KEY (Entrega) REFERENCES ee_Entrega(IDentrega);
alter table agr_Reporteentrega
add constraint FK_Reporteentrega_Productos
foreign key (Producto) references agr_Productos(IDproducto);

-- Tablas intermedias
CREATE TABLE agr_Almacen_Proveedor(
	Proveedor int not null, --pkfk hacia proveedor
	Almacen varchar(5) not null, --pkfk hacia almacen
	Cantidad int not null CHECK(Cantidad > 0), -- check agregado para que no permita valores menores o iguales a 0
	Fechasol date not null
);
alter table agr_Almacen_Proveedor
add primary key (Proveedor, Almacen);
alter table agr_Almacen_Proveedor
add constraint FK_Almacen_Proveedor_Proveedor
foreign key(Proveedor) references p_Proveedor(IDproveedor);
alter table agr_Almacen_Proveedor
add constraint FK_Almacen_Proveedor_Almacen
foreign key (Almacen) references agr_Almacen(IDalmacen);

CREATE TABLE agr_Categoria_Producto(
    Categorias varchar(5) not null, --pkfk hacia productos
	Productos int not null --pkfk hacia categoriaproducto
);
ALTER TABLE agr_Categoria_Producto
ADD PRIMARY KEY (Categorias, Productos);
ALTER TABLE agr_Categoria_Producto
ADD CONSTRAINT FK_Categoria_Producto_Productos
FOREIGN KEY (Categorias) REFERENCES agr_Productos(IDproducto);
ALTER TABLE agr_Categoria_Producto
ADD CONSTRAINT FK_Categoria_Producto_Categoriaproducto
FOREIGN KEY (Productos) REFERENCES agr_Categoriaproducto(IDcategoriaprod);

CREATE TABLE ee_Almacen_Paqueteria(
    Almacen varchar(5) not null, --pkfk hacia serviciopaqueteria
	Paqueteria int not null --pkfk hacia almacen
);
ALTER TABLE ee_Almacen_Paqueteria
ADD PRIMARY KEY (Almacen, Paqueteria);
ALTER TABLE ee_Almacen_Paqueteria
ADD CONSTRAINT FK_Almacen_Paqueteria_Almacen
FOREIGN KEY (Almacen) REFERENCES agr_Almacen(IDalmacen);
ALTER TABLE ee_Almacen_Paqueteria
ADD CONSTRAINT FK_Almacen_Paqueteria_Serviciopaqueteria
FOREIGN KEY (Paqueteria) REFERENCES ee_Serviciopaqueteria(IDpaqueteria);

CREATE TABLE agr_Producto_Proveedor(
	Proveedor int not null, --pkfk hacia proveedor
	Producto varchar(5) not null --pkfk hacia productos
);
alter table agr_Producto_Proveedor
add primary key (Proveedor, Producto);
alter table agr_Producto_Proveedor
add constraint FK_Producto_Proveedor_Proveedor
foreign key(Proveedor) references p_Proveedor(IDproveedor);
alter table agr_Producto_Proveedor
add constraint FK_Producto_Proveedor_Producto
foreign key(Producto) references agr_Productos(IDproducto);

CREATE TABLE agr_Provedor_Entrega_Renvio(
    Reporteenvio int not null, --pkfk hacia reporteenvio
	Reporteentrega int not null, --pkfk hacia reporteentrega
	Proveedor int not null --pkfk hacia proveedor
);
ALTER TABLE agr_Provedor_Entrega_Renvio
ADD PRIMARY KEY (Reporteenvio, Reporteentrega, Proveedor);
ALTER TABLE agr_Provedor_Entrega_Renvio
ADD CONSTRAINT FK_Provedor_Entrega_Renvio_Reporteenvio
FOREIGN KEY (Reporteenvio) REFERENCES agr_Reporteenvio(IDreporteenv);
ALTER TABLE agr_Provedor_Entrega_Renvio
ADD CONSTRAINT FK_Provedor_Entrega_Renvio_Reporteentrega
FOREIGN KEY (Reporteentrega) REFERENCES agr_Reporteentrega(IDreportent);
ALTER TABLE agr_Provedor_Entrega_Renvio
ADD CONSTRAINT FK_Provedor_Entrega_Renvio_Proveedor
FOREIGN KEY (Proveedor) REFERENCES p_Proveedor(IDproveedor);

CREATE TABLE agr_Producto_Almacen_Rpro(
	Almacen varchar(5) not null, --pkfk hacia almacen
	Producto varchar(5) not null, --pkfk hacia producto
	ReporteProducto int not null, --pkfk hacia reporteproducto
	Estatus varchar(20) not null,
	CHECK (Estatus IN('Disponible','No disponible'))
);
alter table agr_Producto_Almacen_Rpro
add primary key(Almacen, Producto, ReporteProducto);
ALTER TABLE agr_Producto_Almacen_Rpro
ADD CONSTRAINT FK_Producto_Almacen_Rpro_Almacen
FOREIGN KEY (Almacen) REFERENCES agr_Almacen(IDalmacen);
ALTER TABLE agr_Producto_Almacen_Rpro
ADD CONSTRAINT FK_Producto_Almacen_Rpro_Productos
FOREIGN KEY (Producto) REFERENCES agr_Productos(IDproducto);
ALTER TABLE agr_Producto_Almacen_Rpro
ADD CONSTRAINT FK_Producto_Almacen_Rpro_Reporteproducto
FOREIGN KEY (ReporteProducto) REFERENCES agr_Reporteproducto(IDreporteprod);

----------------------------------------------------------

















